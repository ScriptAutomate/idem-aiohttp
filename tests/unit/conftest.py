import aiohttp.web
import pytest


@pytest.fixture(scope="session", name="hub")
def unit_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="function", autouse=True)
async def cleanup_hub(hub):
    hub.tool.http.application.APP.freeze()
    await hub.tool.http.application.APP.shutdown()
    hub.tool.http.application.APP = aiohttp.web.Application()


@pytest.fixture(scope="session")
def acct_subs():
    return ["http"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_idem_aiohttp"
