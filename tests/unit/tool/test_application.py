from unittest import mock

import aiohttp.web
import pytest


async def test_client(hub, ctx):
    mock_client = mock.MagicMock()
    connector = await hub.tool.http.application.connector(ctx, mock.MagicMock)
    cookie_jar = await hub.tool.http.application.cookie_jar(ctx, mock.MagicMock)

    await hub.tool.http.application.client(ctx, mock_client)

    mock_client.assert_called_once_with(
        auth=None,
        connector_owner=False,
        connector=connector,
        cookie_jar=cookie_jar,
        headers={},
    )


async def test_resolver(hub, ctx):
    mock_resolve = mock.MagicMock()
    ret = await hub.tool.http.application.resolver(ctx, mock_resolve)
    assert ret

    mock_resolve.assert_called_once_with()


async def test_connector(hub, ctx):
    mock_connector = mock.MagicMock()
    resolver = await hub.tool.http.application.resolver(ctx, mock.MagicMock)
    ret = await hub.tool.http.application.connector(ctx, mock_connector)
    assert ret

    mock_connector.assert_called_once_with(resolver=resolver)
